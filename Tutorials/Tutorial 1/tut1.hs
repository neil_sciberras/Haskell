module Class where 

--1
circleArea :: Double -> Double 
circleArea radius = pi * radius * radius

--2
dbl :: Integer -> Integer
dbl x = 2*x

quad :: Integer -> Integer
quad x = 2*(dbl x)

--3
sumN :: Integer -> Integer
sumN n 
 |n==0 = 0
 |otherwise = n + sumN (n-1)
 
--4 
type Distance = Double 
type Cartesian = (Distance, Distance)

--a
type Rectangle = (Cartesian, Cartesian)

--assuming that the rectangle is not diagonal 

--b i
lengthR :: Rectangle -> Distance
lengthR ((_, y0),(_, y1))  = y1-y0 

--ii
breadthR :: Rectangle -> Distance
breadthR ((x0, _),(x1, _))  = x1-x0

--iii
topLeft :: Rectangle -> Cartesian
topLeft ((x0, y0), (x1, y1)) = (x0, y1)

botLeft :: Rectangle -> Cartesian
botLeft ((x0, y0), (x1, y1)) = (x0, y0)

topRight :: Rectangle -> Cartesian
topRight ((x0, y0), (x1, y1)) = (x1, y1)

botRight :: Rectangle -> Cartesian
botRight ((x0, y0), (x1, y1)) = (x1, y0)

--iv
liesIn :: Cartesian -> Rectangle -> Bool
liesIn (x,y) ((x1,y1), (x2, y2)) = (x <= x2 && x >= x1) && (y <= y2 && y >= y1) 

--c
areaR :: Rectangle -> Double
areaR r = lengthR r * breadthR r

--d

intersect :: Rectangle -> Rectangle -> Bool 
intersect r1 r2 = (botLeft r1) `liesIn` r2
 || (botRight r1) `liesIn` r2 
 || (topLeft r1) `liesIn` r2 
 || (topRight r1) `liesIn` r2 
 || (botLeft r2) `liesIn` r1 
 || (botRight r2) `liesIn` r1 
 || (topLeft r2) `liesIn` r1 
 || (topRight r2) `liesIn` r1
 
--5

type Trapezoid = (Double, Double, Double)

--a
areaOT :: Trapezoid -> Double
areaOT (h1, h2, breadth) = breadth * ((h1+h2))/2

--b 
approxArea1 :: (Double -> Double) -> Double 
approxArea1 f = (f(0)+f(1))/2


--c 
--"approx1 (*2) 1 3" gives "8.0"
approx1 :: (Double -> Double) -> Double -> Double-> Double 
--             function          left       rigth    result
approx1 f lb rb = (rb-lb)*((f(lb)+f(rb))/2)

--d
--"approx2 (*2) 1 3" still gives "8.0"
approx2 :: (Double -> Double) -> Double -> Double -> Double 
approx2 f lb rb = (approx1 f lb middle) + (approx1 f middle rb)
 where
  middle = lb + ((rb-lb)/2)

--e  
approxArea :: Double -> Double -> Double -> (Double -> Double) -> Double
--               E        l          r            function           Area
approxArea e l r f 
 |abs (l-r) < e = approx1 f l r
 |otherwise     = (approxArea e l mid f) + (approxArea e mid r f)
 where 
  mid = (l+r)/2