module Tutorial2 where 

import Data.List
import Data.Char

--num1
--Doubles all the elements.
doubleElts :: [Integer] -> [Integer]
doubleElts [] = []
doubleElts (x:xs) = 2*x : doubleElts xs

--using list comprehension
doubleElts' :: [Integer] -> [Integer]
doubleElts'  xs = [2*x| x <- xs]
--list xs is dumped one by one into the variable x,
--and then a new list is generated.


--num2
--Returns the sum of all the elements.
sumList :: [Integer] -> Integer
sumList [] = 0
sumList (x:xs) = x + sumList xs

{--
--just experimenting
sumList' :: (a -> b -> a) -> a -> [b] -> a
sumList' f a0 [] = a0 
sumList' f a0 [b:bs] = 
--}

--using the foldl
sumList'' :: [Integer] -> Integer
sumList'' xs = foldl (+) 0 xs

--num3
--Returns the last element of the list.
lastElt :: [a] -> a
lastElt [x] = x
lastElt (x:xs) = lastElt xs 

--num4 
--takeN 2 [1,2,3] = [1,2]
takeN :: Int -> [a] -> [a]
takeN n [] = []
takeN 0 (x:xs) = []
takeN n (x:xs) 
 |length (x:xs) < n = (x:xs)
 |otherwise = x: takeN (n-1) xs 

--dropN 2 [1,2,3] = [3]
dropN :: Int -> [a] -> [a]
dropN n [] = []
dropN 0 xs = xs 
dropN n (x:xs) 
 |length (x:xs) < n = []
 |otherwise = dropN (n-1) xs


--num5
-- [1,2,3] join ['a','b','c'] = [(1,'a'),(2,'b'),(3,'c')]
join :: [a] -> [b] -> [(a,b)]
join [] _ = []
join _ [] = []
join (x:xs) (y:ys) = (x,y): join xs ys

--num6
--Returns the odd positioned elements of the list 
oddElts :: [a] -> [a]
oddElts [] = []
oddElts [x] = [x]
oddElts (x:y:xs) = x: oddElts xs

--num7
--A
--Removes the initial whitespace from the beginning of the string.
skipWhiteSpace :: [Char] -> [Char]
skipWhiteSpace [] = []
skipWhiteSpace (x:xs) 
 | isSpace x = xs
 | otherwise = x: skipWhiteSpace xs

--B
--returns 2 strings.
--i) The first word in the original string
--ii) The remaining of the string 
getWord :: [Char] -> ([Char], [Char])
getWord [] = ([],[])
getWord (x:xs) 
 | isSpace x = getWord (skipWhiteSpace (x:xs))
 | otherwise = break' isSpace (x:xs)


--got this from lect2.hs. This is being used in the definition 
--of getWord.
break' :: (Char -> Bool) -> [Char] -> ([Char], [Char])
break' p [] = ([], [])
break' p (x:xs)
 | p x = ([], x:xs)
 | otherwise = 
  let (ys, zs) = break' p xs 
  in (x:ys, zs)

--C
--Turns one whole string into a number of strings, seperating word by word.
intoWords :: [Char] -> [[Char]]
intoWords [] = []
intoWords (x:xs)
 | isSpace x = intoWords (skipWhiteSpace (x:xs))
 | otherwise = 
  let (ys, zs) = getWord (x:xs) --since getWord returns a pair of 2 lists, we let (ys,zs) represent that pair.
  in (ys: intoWords zs)

{--  
--D
--["The", "sky", "is", "not", "blue"] 12 = ("The sky is", ["not", "blue"])
getLine :: [[Char]] -> Integer -> ([Char], [[Char]])
getLine x:xs 0 = ([], x:xs)
getLine [] width = ([],[])
getLine x:xs width 
 | length x <= width =  
  let (ys, z:zs) = (getLine xs (width-((length x)+1)))
  in (x ++ " ", [ys, z:zs])
 | otherwise 
  
--}  
  