module Tutorial2 where 

import Data.List

--1A
--We are defining new datatypes her.
--The show and eq, gives some functionality to the data type you are 
--defining. 'Eq' for example, gives the functionality that you can equate
--the items of the data type.
data ChessPiece = King | Queen | Bishop | Knight | Rook | Pawn 
 deriving (Eq) --if you remove the ChessPiece from being an instance of show,
 --which was declared deeper in the code, then the deriving statement should be: 
 -- "deriving (Show, Eq)"

data DraughtsPiece = Normal | Promoted 
 deriving (Show, Eq)

--1B
valueChess :: ChessPiece -> Float
valueChess King = 0
valueChess Queen = 9 
valueChess Bishop = 3.5
valueChess Knight = 3
valueChess Rook = 5
valueChess Pawn = 1

valueDraughts :: DraughtsPiece -> Float
valueDraughts Normal = 1
valueDraughts Promoted = 3


--1C
--The 'a' is used so the square can be occupied by both a chess and a 
--draughts piece. Example 'BlackP King'
data Square a = Empty | BlackP a | WhiteP a
 deriving (Show, Eq)

--1D
occupied :: Square a -> Bool
occupied Empty = False
occupied _ = True --it doesnt matter if its black or white, it's still occupied
 
--the maybe gives the functionality to encode the emptiness
--of an object. Later we can start checking for emptiness using the 
--type maybe, ex. 'IsNothing'
piece :: Square a -> Maybe a 
piece Empty = Nothing
piece (BlackP x) = Just x
piece (WhiteP x) = Just x

data Owner = Black | White

--returns if the piece is black or white
owner' :: Square a -> Maybe Owner
owner' Empty = Nothing
owner' (BlackP _) = Just Black
owner' (WhiteP _) = Just White

valueSquareChess :: Square ChessPiece -> Float
valueSquareChess Empty = 0
valueSquareChess (BlackP x) = negate (valueChess x)
valueSquareChess (WhiteP x) = valueChess x
 
valueSquareDraughts :: Square DraughtsPiece -> Float
valueSquareDraughts Empty = 0
valueSquareDraughts (BlackP x) = negate (valueDraughts x)
valueSquareDraughts (WhiteP x) = valueDraughts x

--1F
data Board a = BRD [[Square a]]
--This is not a type synonym, but a new data type. 
--Usually there is elements divided by pipes, those are constructors,
--our constructor here is BRD. BRD is similar to WhiteP.
--It could have been data Board a = BRD [[Square a]] | BRD2 [[Square a]]
 
--1G
scoreChess :: Board ChessPiece -> Float
scoreChess (BRD xs) = sum (map (sum . map valueSquareChess) xs)

--scoreDraughts :: Board DraughtsPiece -> Float

{--
toScore :: [[Square ChessPiece]] -> [[Float]]
toScore xs = map(map valueSquareChess) xs
--toScore xs = sum( map(sum . map valueSquareChess) xs)
--toScore (x:xs) = map (valueSquareChess (x))
--}

--this is just a board to be able to test scoreChess and scoreDraughts
b1 = BRD [[Empty,WhiteP Queen,Empty,Empty,Empty,Empty,Empty,Empty],
      [Empty,Empty,Empty,Empty,Empty,Empty,Empty,Empty],
      [Empty,Empty,Empty,Empty,Empty,Empty,Empty,Empty],
      [Empty,Empty,Empty,Empty,Empty,Empty,Empty,Empty],
      [Empty,Empty,Empty,Empty,Empty,Empty,Empty,Empty],
      [Empty,Empty,Empty,Empty,Empty,Empty,Empty,Empty],
      [Empty,Empty,Empty,Empty,Empty,Empty,Empty,Empty],
      [Empty,Empty,Empty,Empty,Empty,Empty,Empty,Empty]]
	  
	  

	  
	  
	  
--QUESTION 2

--A : making all types in 1, instances of the Show typeclass
{-
data ChessPiece = King | Queen | Bishop | Knight | Rook | Pawn
data DraughtsPiece = Normal | Promoted 
data Square a = Empty | BlackP a | WhiteP a
data Owner = Black | White
data Board a = BRD [[Square a]]
-}
instance Show ChessPiece where 
 show King = "King"
 show Queen = "Queen"
 show Bishop = "Bishop"
 show Knight = "Knight"
 show Rook =  "Rook"
 show Pawn = "Pawn"

instance Show DraughtsPiece where 
 show Normal = "Normal"
 show Promoted = "Promoted"

instance Show Square a where 
 show Empty = "Empty"
 show BlackP a = "BlackP" ++ show a
 show WhiteP a = "WhiteP" ++ show a
 
 
 
 
 
 