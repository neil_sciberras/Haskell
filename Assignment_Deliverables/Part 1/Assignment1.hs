module AssignmentNum1 where 

import ImageIO

--defining some useful colours
black, white, shockingPink, red :: RGB
black = (0,0,0)
white = (1,1,1)
shockingPink = (0.99,0.06,0.75)
red = (1,0,0)

  
  
--Number 1
--produces a striped image of 100x100, with the odd columns red,
--and the even ones shocking pink. (Columns start from 1, odd, hence red)

stripes :: Image
stripes = ((100,100), pixels1)
 where 
  pixels1 (x,y)
   |(x) `mod` 2 == 0 = red   --when x=0, 0 mod 2 == 0, since column 1 is at x=0
   |otherwise = shockingPink
   
   
q_1 =
 writeImage "out-1.png" stripes
 

--Number 2
 
flipVertically :: Image -> Image
flipVertically ((w,h), pixels2) = ((w,h), pixels2')
 where 
  pixels2' (x,y) = pixels2 (x, (h-1-y))
  --the x values are left intact, while the y values are flipped
  {--
  Assume an image of height 8
  @ y=0 -> 8-1-0=7
  @ y=1 -> 8-1-1=6
  .
  .
  @ y=6 -> 8-1-6=1
  @ y=7 -> 8-1-7=0
  --}

q_2 = 
 do
  img2 <- readImage "in.png"
  writeImage "out-2.png" (flipVertically img2)
  

 
--Number 3

--Image :: (Coord, Coord -> RGB)

darken :: Float -> Image -> Image
darken n ((w,h), pixels3) = ((w,h), pixels3')
 where
  pixels3' (x,y) =
   let (r,g,b) = pixels3 (x,y)
    in ((n*r),(n*g),(n*b))

q_3 = 
 do
  img3 <- readImage "in.png"
  writeImage "out-3.png" (darken 0.25 img3)


--Number 4
--brings a colour to greyscale

desaturate :: RGB -> RGB
desaturate (r,g,b) = (x,x,x)
 where 
  x = (r+g+b)/3
  
q_4 = 
 do
  writeImage "test-4.png" ((100,100), pixels4)
   where 
    pixels4 (x,y) = desaturate red


--Number 5
--brings a whole image to grayscale not just an RGB pixel.
--Using the function defined in 4, to desaturate the colour of the image. 
desaturateImage :: Image -> Image
desaturateImage ((w,h), pixels5) = ((w,h), pixels5')
 where 
  pixels5' (x,y) = (desaturate (pixels5 (x,y)))
  --applying desaturate function on every pixel in this image.


q_5 = 
 do
  img5 <- readImage "in.png"
  writeImage "out-5.png" (desaturateImage img5)
  
  
--Number 6
--Desaturates all colours except the one passed as parameter. 
--The similarity of the specified colour with all the pixels is done using 
--a function colourDistance from ImageIO library. 

--If a pixel's distance from red is more than 0.1, then it is saturated. 
--Otherwise, it is left as is. 

selectiveDesaturateImage :: RGB -> Image -> Image 
selectiveDesaturateImage (r,g,b) ((w,h), pixels6) = ((w,h), pixels6')
 where 
  pixels6' (x,y) 
   --If the 2 rgb pixels (the one we want to retain and the general 
   --pixel in the image) have distance smaller than 0.1, then leave it 
   --intact by using the old pixel function. 
   | colourDistance (r,g,b) (pixels6 (x,y)) <= 0.1 = pixels6 (x,y) 
   --If their distance is greater than 0.1, then desaturate it using the 
   --desaturate function. 
   | otherwise = desaturate (pixels6 (x,y))

q_6 = 
 do
  img6 <- readImage "in.png"
  writeImage "out-6.png" (selectiveDesaturateImage red img6)