--This module was provided by the lecturer (not my work)

module ImageIO (RGB, Coordinates, Image, (~>), readImage, writeImage, colourDistance) where

import qualified Codec.Picture as Picture

type RGB = (Float, Float, Float)
type Coordinates = (Integer, Integer)
type Image = (Coordinates, Coordinates -> RGB)

-- Factor an integer by a float
(~>) :: (RealFrac a, Integral b) => a -> b -> b
factor ~> value = floor $ factor * fromIntegral value

-- Colour similarity
type HSV = (Float, Float, Float)

colourDistance :: RGB -> RGB -> Float
colourDistance c1 c2 = (min delta (360-delta) / 360)
  where
    (h1,_,_) = rgb2hsv c1
    (h2,_,_) = rgb2hsv c2

    delta = abs (h1 - h2)

    rgb2hsv :: RGB -> HSV
    rgb2hsv (0,0,0) = (0,0,0)
    rgb2hsv (r,g,b) = (h,s,v)
      where
        smallest = minimum [r,g,b]
        biggest = maximum [r,g,b]
        delta = biggest - smallest

        v = biggest
        s = delta / biggest
        h'
          | delta == 0   = 0
          | r == biggest = 60 * ((g-b)/delta + 0)
          | g == biggest = 60 * ((b-r)/delta + 2)
          | otherwise    = 60 * ((r-g)/delta + 4)
        h = if h' < 0 then h' + 360 else h'


-- Convert assignment representation into Codec.Picture format
myimage2image :: Image -> Picture.Image Picture.PixelRGB8
myimage2image ((width, height), pixels) =
  Picture.generateImage pixelRGB8At (fromIntegral width) (fromIntegral height)
  where
    pixelRGB8At :: Int -> Int -> Picture.PixelRGB8
    pixelRGB8At x y =
      let (r,g,b) = pixels (toInteger x, toInteger y)
      in  Picture.PixelRGB8 (toWord8 r) (toWord8 g) (toWord8 b)

    toWord8 v = min 255 (fromIntegral $ v ~> (255 :: Integer))

-- Convert Codec.Picture format into assignment representation
image2myimage :: Picture.Image Picture.PixelRGB8 -> Image
image2myimage image = ((w,h), pixels)
  where
    w = toInteger $ Picture.imageWidth image
    h = toInteger $ Picture.imageHeight image

    pixels (x,y) =
      let (Picture.PixelRGB8 r g b) = Picture.pixelAt image (fromInteger x) (fromInteger y)
      in  (fromIntegral r / 255, fromIntegral g / 255, fromIntegral b / 255)

-- Read a file into an image using the assignment representation
readImage :: String -> IO Image
readImage png_filename =
  do
    image <- Picture.readPng png_filename
    case image of
      (Right dynamic_image) -> return (image2myimage $ Picture.convertRGB8 dynamic_image)
      (Left error_message)  -> fail error_message

-- Write a file into an image using the assignment representation
writeImage :: String -> Image -> IO ()
writeImage png_filename = Picture.writePng png_filename . myimage2image

