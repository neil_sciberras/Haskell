module Assign2 where 

import ImageMapIO

red, blue, darkBlue, black, white, shockingPink :: RGB
red = (1,0,0)
blue = (0,0,1)
darkBlue = (0,0,0.2)
black = (0,0,0)
white = (1,1,1)
shockingPink = (0.99, 0.06, 0.75)

--QUESTION 1
--Returns the width of the image, which is the same as the length of 
--one of the lines (length of a list of RGBs)
width :: ImageMap -> Int
width image = length (head image)

q_1 =
  do
    image1 <- readImageMap "in.png"
    putStrLn (show (length image1))

--QUESTION 2
--Flipping vertically is the same as reversing the order of the 
--lines. To reverse the lines, we are reversing the outer list, which in it
--contains the other lists of RGBs. 
flipVertically :: ImageMap -> ImageMap
flipVertically image = reverse image

q_2 =
  do
    image2 <- readImageMap "in.png"
    writeImageMap "out-2.png" (flipVertically image2)


--this was not required, but is going t be used in question 9
flipHorizontally :: ImageMap -> ImageMap
flipHorizontally = map reverse


--QUESTION 3
--The number of lines to be created is 'y'. These are created by mapping the 'line' 
--function on a list containing the line numbers from 1 to y.
--According to the value of n given to 'line', it decides whether its line number is 
--odd or even. This is checked so as odd and even lines start with alternating colours.
--Then each line is rendered using 2 functions. 
--'renderLineOdd' is given the size of the line, and it creates a list of RGBs (a line).
--Using guards, the alternating sequence of colours within the line is created. 
--'renderLineEven' renders the line so as to be in an alternating manner to the line above
--which is odd. 

chequered :: Coordinates -> ImageMap
chequered (x,y) = map line [1..y]
 where 
  line n = if n `mod` 2 == 1 then oddline else evenline
  oddline = renderLineOdd x
  evenline = renderLineEven x

renderLineOdd :: Int -> [RGB]
renderLineOdd n 
 | n == 0 = []
 | n `mod` 2 == 1 = (blue: renderLineOdd (n-1))
 | otherwise = (red: renderLineOdd (n-1))

renderLineEven :: Int -> [RGB]
renderLineEven n 
 | n == 0 = []
 | n `mod` 2 == 0 = (blue: renderLineEven (n-1))
 | otherwise = (red: renderLineEven (n-1)) 
 
q_3 =
  do
    writeImageMap "out-3.png" (chequered (100,100))  

--QUESTION 4

--redden multiplies the red factor by 1.5, such that it does not exceed 1.
--This is applied on single pixels.
redden :: RGB -> RGB
redden (r,g,b) 
 | 1.5*r > 1 = (1,g,b)
 | otherwise = ((1.5*r),g,b)

--this is the same function from assignment 1, which desaturates one pixel at a time.
desaturatePixel :: RGB -> RGB
desaturatePixel (r,g,b) = (x,x,x)
 where 
  x = (r+g+b)/3 

--Fades a pixel by the factor passed as argument, by multiplying each colours
--component by the factor passed as argument.
fade :: Float -> RGB -> RGB
fade n (r,g,b) = ((r*n), (g*n), (b*n))


--QUESTION 5

--It is mapping 'map f' onto all the lines. All the lines are being represented
--by 'im' (the image). Then, what 'map f' is doing, it is mapping 'f'
--onto all the elements in the lines, which are RGBs.
recolourImage :: (RGB -> RGB) -> ImageMap -> ImageMap
recolourImage f im =  map (map f) im

reddenImage :: ImageMap -> ImageMap
reddenImage img = map (map redden) img

desaturateImage :: ImageMap -> ImageMap
desaturateImage img1 = map (map desaturatePixel) img1

fadeImage :: Float -> ImageMap -> ImageMap
fadeImage n img2 = map (map (fade n)) img2

--reddening an image using the 'recolourImage' function and passing
--'redden' as a parameter.
q_5a =
  do
    image5 <- readImageMap "in.png"
    writeImageMap "out-5a.png" (recolourImage redden image5)
--desaturating an image by passing 'desaturatePixel' as a parameter to 
--recolourImage.
q_5b =
  do
    image5 <- readImageMap "in.png"
    writeImageMap "out-5b.png" (recolourImage desaturatePixel image5)

--reddening an image by using the 'redden' image defined.
q_5c =
  do
    image5 <- readImageMap "in.png"
    writeImageMap "out-5c.png" (reddenImage image5)

--desaturating an image by applying 'desaturateImage' on the image.
q_5d =
  do
    image5 <- readImageMap "in.png"
    writeImageMap "out-5d.png" (desaturateImage image5)

--fading an image by applying the 'fadeImage' function.
q_5e =
  do
    image5 <- readImageMap "in.png"
    writeImageMap "out-5e.png" (fadeImage 0.5 image5)

{--
--to desaturate a line, desaturatePixel is mapped on the line, where a line
--is a list of pixles. 
desatLine :: [RGB] -> [RGB]
desatLine xs = map desaturatePixel xs
--}

{--
--This is the function that desaturates the whole image. 
--An image is a list of lists. 
--So we are mapping desatLine on the list of lines, and then desatLine maps the function that 
--desaturates the pixles on the line itself, which desaturates the pixels.
desaturate :: ImageMap -> ImageMap
desaturate img = map desatLine img
--}
  

--QUESTION 6
overlap :: RGB -> RGB -> RGB
overlap (r1,g1,b1) (r2,g2,b2) = (((r1+r2)/2), ((g1+g2)/2), ((b1+b2)/2))
  
--QUESTION 7
--Combining 2 lines, by applying the combining function on the heads of the 
--2 lists/lines, and then calling 'combineLine' recursively on the tail
--of both lists. (it is how the 'map' works)
--Here x and y, are single RGBs.
combineLine :: (RGB -> RGB -> RGB) -> [RGB] -> [RGB] -> [RGB]
combineLine f [] _ = []
combineLine f _ [] = []
combineLine f  (x:xs) (y:ys) = f x y:combineLine f xs ys

--Here x and y, are single lines. Because ImageMap is a list of lines.
combineImages :: (RGB -> RGB -> RGB) -> ImageMap -> ImageMap -> ImageMap
combineImages f [] _ = []
combineImages f _ [] = []
combineImages f (x:xs) (y:ys) =  combineLine f x y:combineImages f xs ys

--This function was just created to test out that the 'combineImages' work.
--maxComb just keeps the highest component between the two pixels.
maxComb :: RGB -> RGB -> RGB
maxComb (r1,g1,b1) (r2,g2,b2) = (maximum [r1, r2], maximum [g1, g2], maximum [b1, b2])

--Testing the 'combineImages' by applying the 'max' function.
q_7a = do
 image71 <- readImageMap "in.png"
 image72 <- readImageMap "q_2.png"
 writeImageMap "out-7a.png" (combineImages max image71 image72)

--Testing the 'combineImages' by applying the 'overlap' function.
q_7b = do
 image71 <- readImageMap "in.png"
 image72 <- readImageMap "q_2.png"
 writeImageMap "out-7b.png" (combineImages overlap image71 image72)

--QUESTION 8
--This is just like 'combineImages', but hardcoded for the 'overlap' function,
--not any function. 
(+++) :: ImageMap -> ImageMap -> ImageMap
(+++) [] _ = []
(+++) _ [] = []
(+++) (x:xs) (y:ys) =  combineLine overlap x y:combineImages overlap xs ys

q_8 = do
 image81 <- readImageMap "in.png"
 image82 <- readImageMap "q_2.png"
 writeImageMap "out-8.png" ((+++) image81 image82)


--QUESTION 9
--This function uses '(+++)' to overlap an imgae onto its flipVertically, flipHorizontally, and flipHorizontally&Vertically altogether.
--'(+++)' takes 2 images as parameters. 
--(from right to left) 
--1st (+++): overlapping flipHorizontally and flipVertically.
--2nd (+++): overlapping flipHorizontally&Vertically and the overlapped output of the 1st (+++).
--3rd (+++): overlapping the original 'img' with the output of the already overlapped images.

kaleidoscope :: ImageMap -> ImageMap
kaleidoscope img = (+++) img ((+++) (flipVertically (flipHorizontally (img))) ((+++) (flipHorizontally (img)) (flipVertically (img))))

q_9 = do
 image9 <- readImageMap "in.png"
 writeImageMap "out-9.png" (kaleidoscope image9)