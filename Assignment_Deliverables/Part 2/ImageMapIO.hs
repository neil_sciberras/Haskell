--This module was provided by the lecturer (not my work)

module ImageMapIO (RGB, ImageMap, Coordinates, readImageMap, writeImageMap) where

import Codec.Picture

type RGB = (Float, Float, Float)
type ImageMap = [[RGB]]
type Coordinates = (Int, Int)

-- Factor an integer by a float
(~>) :: (RealFrac a, Integral b) => a -> b -> b
factor ~> value = floor $ factor * fromIntegral value

width, height :: ImageMap -> Int
height = length
width = length . head

-- Convert to internal format
imagemap2image :: ImageMap -> Image PixelRGB8
imagemap2image imagemap =
  snd $ generateFoldImage calculatePixel (concat imagemap) (width imagemap) (height imagemap)
  where
    calculatePixel (p:ps) _ _ = (ps, toPixelRGB8 p)
    toPixelRGB8 (r,g,b) = PixelRGB8 (toWord8 r) (toWord8 g) (toWord8 b)

    toWord8 v = min 255 (fromIntegral $ v ~> (255 :: Integer))

image2imagemap :: Image PixelRGB8 -> ImageMap
image2imagemap image =
  [ [ toRGB (pixelAt image x y) | x <- [0..w-1] ] | y <- [0..h-1] ]
  where
    w = imageWidth image
    h = imageHeight image

    toRGB (PixelRGB8 r g b) = (float r, float g, float b)
    float v = fromIntegral v / 255

readImageMap :: String -> IO ImageMap
readImageMap png_filename =
  do
    image <- readPng png_filename
    case image of
      (Right dynamic_image) -> return (image2imagemap $ convertRGB8 dynamic_image)
      (Left error_message)  -> fail error_message

writeImageMap :: String -> ImageMap -> IO ()
writeImageMap png_filename = writePng png_filename . imagemap2image

