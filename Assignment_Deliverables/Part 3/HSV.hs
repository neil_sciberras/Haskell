--This module was provided by the lecturer (not my work)

module HSV (HSV (HSV), rgb2hsv, hsv2rgb) where

import QuadImageIO

newtype HSV = HSV (Float, Float, Float) deriving (Eq, Show)

rgb2hsv :: RGB -> HSV
rgb2hsv (RGB (0,0,0)) = HSV (0,0,0)
rgb2hsv (RGB (r,g,b)) = HSV (h,s,v) 
  where 
    smallest = minimum [r,g,b]
    biggest = maximum [r,g,b]
    delta = biggest - smallest

    v = biggest
    s = delta / biggest
    h' 
      | delta == 0   = 0
      | r == biggest = 60 * (((g-b)/delta) `fmod` 6)
      | g == biggest = 60 * ((b-r)/delta + 2) 
      | otherwise    = 60 * ((r-g)/delta + 4)
    h = if h' < 0 then h' + 360 else h'

hsv2rgb :: HSV -> RGB
hsv2rgb (HSV (h,s,v)) = RGB (r' + m, g' + m, b' + m)
  where
    c = v * s
    x = c * (1 - abs((h/60) `fmod` 2 - 1))
    m = v - c

    (r',g',b')
      | h < 60    = (c,x,0)
      | h < 120   = (x,c,0)
      | h < 180   = (0,c,x)
      | h < 240   = (0,x,c)
      | h < 300   = (x,0,c)
      | otherwise = (c,0,x)

x `fmod` y = (x - fromIntegral (floor x)) + fromIntegral (floor x `mod` y)

