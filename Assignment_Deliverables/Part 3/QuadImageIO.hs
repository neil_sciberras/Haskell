--This module was provided by the lecturer (not my work)

module QuadImageIO (RGB (RGB), Coordinates, QuadTree (Block, Split), Image, pixelAt, readRGBImage, writeRGBImage) where

import qualified Codec.Picture as Picture 

data RGB = RGB (Float, Float, Float) deriving (Show)
type Coordinates = (Int, Int)

instance Eq RGB where
  RGB (r, g, b) == RGB (r', g', b') =
    floor (r * 100) == floor (r'*100) &&
    floor (g * 100) == floor (g'*100) &&
    floor (b * 100) == floor (b'*100)

black :: RGB
black = RGB (0,0,0)

data QuadTree a = 
    Split (QuadTree a) (QuadTree a) (QuadTree a) (QuadTree a)
  | Block a
  deriving (Eq, Show)
  
type Image a = (Coordinates, QuadTree a) 

size :: Image a -> Coordinates
size = fst

pixelAt :: Image a -> Coordinates -> a
pixelAt (_, Block c) _ = c
pixelAt ((w,h), Split nw ne se sw) (x,y)
  | x < midX =
     if y < midY  
       then pixelAt ((midX, midY), nw) (x,y)
       else pixelAt ((midX, h - midY), sw) (x, y - midY)
  | otherwise =  
     if y < midY  
       then pixelAt ((w - midX, midY), ne) (x - midX, y)
       else pixelAt ((w - midX, h - midY), se) (x - midX, y - midY)
  where
    midX = (w+1) `div` 2 
    midY = (h+1) `div` 2 


-- Factor an integer by a float
(~>) :: (RealFrac a, Integral b) => a -> b -> b
factor ~> value = floor $ factor * fromIntegral value


-- Convert assignment representation into Codec.Picture format
myimage2image :: Image RGB -> Picture.Image Picture.PixelRGB8
myimage2image quadtree = 
  Picture.generateImage pixelRGB8At w h
  where
    (w,h) = size quadtree

    pixelRGB8At :: Int -> Int -> Picture.PixelRGB8
    pixelRGB8At x y = 
      let RGB (r,g,b) = pixelAt quadtree (x, y) 
      in  Picture.PixelRGB8 (toWord8 r) (toWord8 g) (toWord8 b)

    toWord8 v = min 255 (fromIntegral $ v ~> (255 :: Integer))

-- Convert Codec.Picture format into assignment representation
image2myimage :: Picture.Image Picture.PixelRGB8 -> Image RGB
image2myimage image = ((w,h), toQuadTree (0,0) (w,h))
  where
    w = Picture.imageWidth image
    h = Picture.imageHeight image

    toFloatRGB :: Picture.PixelRGB8 -> RGB
    toFloatRGB (Picture.PixelRGB8 r g b) = RGB (fromIntegral r / 255, fromIntegral g / 255, fromIntegral b / 255)

    toQuadTree _ (0,_) = Block black
    toQuadTree _ (_,0) = Block black
    toQuadTree (x0,y0) (w,h) 
      | sameColour pixelsInArea = Block (toFloatRGB $ head pixelsInArea)
      | otherwise = 
          Split
            (toQuadTree (x0,y0) (midX, midY)) (toQuadTree (x0+midX,y0) (w - midX, midY))
            (toQuadTree (x0+midX,y0+midY) (w - midX, h - midY)) (toQuadTree (x0,y0+midY) (midX, h - midY))
      where
        sameColour (x:xs) = all (x==) xs

        pixelsInArea = [ Picture.pixelAt image x y | x <- [x0..x0+w-1], y <- [y0..y0+h-1] ]

        midX = (w+1) `div` 2 
        midY = (h+1) `div` 2 

      
-- Read a file into an image using the assignment representation
readRGBImage :: String -> IO (Image RGB)
readRGBImage png_filename =
  do
    image <- Picture.readPng png_filename
    case image of
      (Right dynamic_image) -> return (image2myimage $ Picture.convertRGB8 dynamic_image)
      (Left error_message)  -> fail error_message

-- Write a file into an image using the assignment representation
writeRGBImage :: String -> Image RGB -> IO ()
writeRGBImage png_filename = Picture.writePng png_filename . myimage2image

