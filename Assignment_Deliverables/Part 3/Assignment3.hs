module Assign3 where 

import QuadImageIO
import HSV


--QUESTION 1

--If the quadtree is just a Block a, then it has only one block.
--If it is divided into 4 other blocks, then we check recursively those blocks. 
blockCount :: Image a -> Int
blockCount (_, Block _) = 1
blockCount (s, Split nw ne se sw) = blockCount(s, nw) + blockCount(s, ne) + blockCount(s, se) + blockCount(s, sw)

--This was not required, but just needed it to calculate the number of pixels in an image.
pixelCount :: Image a -> Int
pixelCount ((w,h),_) = w*h


q_1 = 
 do 
  image <- readRGBImage "in.png"
  putStrLn ("Blocks in 'in.png' : " ++ show(blockCount image))
  putStrLn ("Pixels in 'in.png' : " ++ show(pixelCount image))
  
  
  
--QUESTION 2

--Traverses the quadtree, and returns the colour of the very top left pixel.
--If the image has only 1 colour, hence 1 block, then return the colour of the block.
--Else, keep on recursively calling topLeftColour on the nw quad. 
topLeftColour :: Image a -> a
topLeftColour (_, Block a) = a
topLeftColour (s, Split nw ne se sw) = topLeftColour (s, nw)

q_2 = 
 do 
  image <- readRGBImage "in.png"
  putStrLn ("Top-left pixel of 'in.png' is: " ++ show(topLeftColour image))
  

  
--QUESTION 3

--Given a function that tells how each pixel should be recoloured, 
--this function transforms all the pixels in an image to the desired colour.

recolour :: (a -> b) -> Image a -> Image b
recolour f (s, Block colour1) = (s, Block (f colour1)) --since function f is of type (a->b) then it can be applied firectly on the colour of that block
recolour f ((w,h), Split nw ne se sw) = ((w,h), Split nw' ne' se' sw')
 where --recolourQuad is a similar function to recolour but is applied on a quadTree not on an image
  nw' = recolourQuad f nw
  ne' = recolourQuad f ne
  se' = recolourQuad f se
  sw' = recolourQuad f sw
   
   
recolourQuad :: (a -> b) -> QuadTree a -> QuadTree b
recolourQuad f (Block a) = Block (f a)
recolourQuad f (Split q1 q2 q3 q4) = Split (recolourQuad f q1) (recolourQuad f q2) (recolourQuad f q3) (recolourQuad f q4)


--This has the same affect as darkenRed in Example.hs, but this is applied on a pixel.
--This is used to test the recolour function just defined in this question.
darkenRedPixel :: RGB -> RGB
darkenRedPixel (RGB (r,g,b)) = RGB (r*0.5,g,b)


q_3 =
  do
    image <- readRGBImage "in.png"
    writeRGBImage "out-3.png" (recolour darkenRedPixel image)  
  
 
--QUESTION 4

-- *Given in question*
invertRGB :: RGB -> RGB
invertRGB (RGB (r,g,b)) = RGB (1-r,1-g,1-b)

desaturateHSV :: Float -> HSV -> HSV
desaturateHSV factor (HSV (h,s,v)) = HSV (h,s*factor,v)



invertImageRGB :: Image RGB -> Image RGB
invertImageRGB img = recolour invertRGB img
--just passing invertRGB as input to recolour, which then applies it to the 
--whole image. 


desaturateImageRGB :: Float -> Image RGB -> Image RGB
desaturateImageRGB fact imgRGB = recolour hsv2rgb (recolour (desaturateHSV (fact)) (recolour rgb2hsv imgRGB))
--We are using recolour to apply the conversion on the images.The conversion is originally applied to
--one unit of HSV or RGB, but then recolour applies it to the whole image. 
--We are using 3 recolours in total. 
-- TRANSFORMINGrecolour to HSV (DESATURATINGrecolour (TRANSFORMINGrecolour to RGB))
--Desaturating is done on an HSV image, by passing (desaturateHSV (fact)) to recolour, where 
--(desaturateHSV (fact)) is considered as a single function that can be passed to recolour.
--"desaturateHSV (fact)" is in itself an input, let say 'a' to recolour, which then gives you 'b', the 
--desaturated colour.
--our 'a' in "recolour :: (a -> b) -> Image a -> Image b " is the (Float -> HSV) of the desaturateHSV


q_4 =
  do
   image <- readRGBImage "in.png"
   writeRGBImage "out-4a.png" (invertImageRGB image)  
   writeRGBImage "out-4b.png" (desaturateImageRGB 0 image)

{-
rgb2hsv :: RGB -> HSV
hsv2rgb :: HSV -> RGB

recolour :: (a -> b) -> Image a -> Image b
-}



-- QUESTION 5 

optimiseQuadTree :: Eq a => Image a -> Image a
optimiseQuadTree (size, quadtree) = (size, optimiseQuadTree' quadtree)
 where
  optimiseQuadTree' (Block c) = Block c --the block already has 1 colour, can't optimise it further.
  optimiseQuadTree' (Split nw ne se sw)
   | coloursMatch nw' ne' se' sw' = nw' --if the 4 quadtrees are of the same colour, then we can reduce them to just one of nw' se' ..., 
   --beacuse nw' .. theselves are quadtrees, and what we are trying to define with this set of guards is (optimiseQuadTree' quadtree)
   --which is a quadtree as the name implies.
   | otherwise = Split nw' ne' se' sw'
   --if the 4 quadtrees are not of the same colour, then we set the quadtree to "Split nw' ne' se' sw'", where every one of the sub-quadtrees 
   --have optimiseQuadTree' applied on it (by inserting them into a list, and mapping the optimisation function on the list)
    where
     [nw', ne', se', sw'] = map optimiseQuadTree' [nw, ne, se, sw]

coloursMatch (Block c1) (Block c2) (Block c3) (Block c4) = c1==c2 && c2==c3 && c3==c4
coloursMatch _ _ _ _ = False



q_5 = 
 do 
  image <- readRGBImage "in.png"
  putStrLn ("Blocks in unoptimised desaturated 'in.png' : " ++ show(blockCount (desaturateImageRGB 0 image )))
  putStrLn ("Blocks in optimised desaturated 'in.png' : " ++ show(blockCount (optimiseQuadTree (desaturateImageRGB 0 image))))

  

-- QUESTION 6

--Instances of the typeclass ColourModel are referred to by a. 
--Every instance of this class ColourModel, will have a different implementation of these
--functions, but all functions will have the same types.
class ColourModel a where 
 toRGB :: a -> RGB
 fromRGB :: RGB -> a
 
 
 --redden takes a colour, and will output the same type of colour but reddened.
 --Similar for brighten.
 redden :: a -> a
 redden col = fromRGB( redden' (toRGB col))
  where
   redden' (RGB (r,g,b))
    | 1.5*r > 1 = (RGB (1.0,g,b))
    | otherwise = (RGB ((1.5*r),g,b))
 
 
 brighten :: a -> a
 brighten col = fromRGB (brighten' (toRGB col))
  where
   brighten' (RGB (r,g,b)) = (RGB (r',g',b'))
    where
     r' = if 1.5*r > 1 then 1 else 1.5*r
     g' = if 1.5*g > 1 then 1 else 1.5*g
     b' = if 1.5*b > 1 then 1 else 1.5*b
     --these conditions are set so as the value of every colour component 
     --does not exceed 1, else the image will get distorted.




-- QUESTION 7 

instance ColourModel RGB where
 toRGB (RGB (r,g,b)) = (RGB (r,g,b)) --RGB to RGB stays the same 
 fromRGB (RGB (r,g,b)) = (RGB (r,g,b)) --RGB from RGB stays the same


instance ColourModel HSV where
 toRGB = hsv2rgb --just the same as hsv2rgb which is found in the HSV.hs
 fromRGB = rgb2hsv
 
 brighten (HSV (h,s,v)) = (HSV (h,s,v'))
  where
   v' = if 1.5*v > 1 then 1 else 1.5*v

   
   
q_7 =
  do
   image <- readRGBImage "in.png"
   --a: redden an RGB image, using the recolour image to apply the 
   --redden function on the whole image.
   --Also testing the functions: toRGB and fromRGB which are used in the 
   --redden function.
   writeRGBImage "out-7a.png" (recolour redden image) 
   
   --b: brighten an RGB image, using the recolour image...
   writeRGBImage "out-7b.png" (recolour brighten image)
   
   --c: redden an HSV image, using the recolour image to apply the 
   --redden function on the whole image. The conversions between RGB
   --and HSV are made in the function 'redden' itself.
   writeRGBImage "out-7c.png" (recolour redden image)
   
   --d: brighten HSV image, using the recolour image aswell.
   writeRGBImage "out-7d.png" (recolour toRGB (recolour brighten (recolour rgb2hsv image)))
   
   
   
   
   
   
  