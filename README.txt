Coursework is divided into 3 main parts; Assignment, Tutorials, Lectures.

The work done mainly revolves around basic image processing and manipulation 
using Haskell, by treating the images as different data structures and 
manipulating them accordingly to produce the desired output.  