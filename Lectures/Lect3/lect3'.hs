module DataTypes where 

import Prelude hiding (Maybe(..), Either(..))

--manipulating data types

data Bool3 = T | F | X deriving (Show, Eq)

neg :: Bool3 -> Bool3
neg T = F
neg F = T
neg X = X

(<&>) :: Bool3 -> Bool3 -> Bool3
T <&> T = T
F <&> _ = F
_ <&> F = F
_ <&> _ = X


(<|>) :: Bool3 -> Bool3 -> Bool3
F <|> F = F
T <|> _ = T
_ <|> T = T
_ <|> _ = X

x `nand` y = neg (x <&> y)

tt :: (Bool3 -> Bool3 -> Bool3) -> String
tt f = 
 [ "+---+---+-------+"
 , "| P | Q | f(P,Q) "
 , "+---+---+-------+"
 ] ++
 ["| " ++ show x ++" | "++ show y ++" | " ++ show (f x y) ++ "   |"
 | x <- [T,F,X], y <- [T,F,X]
 ] ++ 
 ["+---+---+-------+"]
 --input: putStrLn (tt nand)

--like in the discrete maths 2 course
data Process 
 = Execute Int
 | Combine Int [Process]
 deriving (Eq, Show)

--sequence
sq :: Prelude -> Int
sq (Execute cost) = cost
sq (Combine cost processes) = cost + sum (map sq processes)
--map will give me a list of values

--alternative definition:
--foldl (\c p -> c+sq p) cost

--parallel processing
pr :: Process -> Int
pr (Execute c) = c
pr (Combine c ps) = c + maximum (map pr ps)
--ex. pr (Combine 9 [Combine 2 [Execute 7, Execute 3, Execute 1], Execute 5]) which will be equal to 27

--example tree of integers
data Tree a = Nil | Node a (Tree a) (Tree a)
 deriving (Eq, Show)
 
 
depth Nil = 0
depth (Node _ lt rt) = 1 + maximum (depth lt) (depth rt)

size Nil = 0
size (Node _ lt rt) = 1 + size lt + size rt

infixT :: Tree a -> [a]
infixT Nil = []
infixT (Node x lt rt) = infixT lt ++ x ++ infixT rt

--if a is an ordered type, which means that '<' can be applied on it, then this is the definition of the insertT function. 
--when the type is not ordered, then this function cannot be applied on that tree.
--its like a guard on the type.
insertT :: Ord a => Tree a -> a -> Tree a
insertT Nil x = Node x Nil Nil
insertT (Node y lt rt) x 
 | x < y     = Node y (insertT lt x) rt
 | otherwise = Node y lt (insertT rt x)
 
--ord a and eq a is saying that the items can be ordered, and that the equality of items can be checked.
--really and truly, we can use ord a only. because if we can check whether an item is smaller or greater than another, than we know 
--how to check their equality.
contains :: (Ord a, Eq a) => Tree a -> a -> Bool
Nil `contains` _ = False
(Node x lt rt) `contains` y
 | x == y    = True
 | x < y     = lt `contains` y
 | otherwise = rt `contains` y
 
list2tree [] = Nil
list2tree (x:xs) = insertT (list2tree xs) x

data Maybe a = Nothing | Just a 
 deriving (Eq, Show)

isNothing Nothing = True 
isNothing _ = False

fromJust (Just x) = x

safediv x 0 = Nothing
safediv x y = Just (x / y)

--turns a function into a safe one. 
safeUnary :: (a -> b) -> Maybe a -> Maybe b
safeUnary f Nothing = Nothing
safeUnary f (Just x) = Just (f x)

--examples
res = safeUnary (1+) (safediv 7 0)