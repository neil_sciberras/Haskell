module Bitmaps where

import Data.List

type Pixel = Bool
type Line = [Pixel]
type Bitmap = [Line]

--square
chessboard size = map chessboardLine [1..size]
 where 
  chessboardLine n = if n `mod` 2 == 1 then oddline else evenline
  oddline = [x `mod` 2 == 1| x <- [1..size]]
  evenline = map not oddline --you could have done it like oddline but x `mod` 2 == 0  

height :: Bitmap -> Int
height bm = length bm --the number of lists of pixels
-- height = length
width = length . head
--width bm = length (head bm) --lenght of one of the lines, where every line is a list


display :: Bitmap -> String
display bm = concat (map displayLine bm) --concat to put them all in one string
 where 
  displayLine :: Line -> String
  displayLine line = 
   map (\b -> if b then '*' else '.') line ++ "\n"
   
readBitmat :: [String] -> Bitmap
readBitmat ss = map (map (\c -> c=='*')) ss --map on everyline the function. 
--the function is checking every caracter, if it is * then true, else false is given.
--readBitmat dinosaur
-- use putStr to avoid seeing '\n'

flipV, flipH :: Bitmap -> Bitmap
flipV = reverse
flipH = map reverse

rotC90 bitmap 
 | width bitmap == 0 = []
 | otherwise = reverse (map head bitmap): rotC90 (map tail bitmap)
 
rot180 = rotC90 . rotC90

--compressed bitmap
type CBitmap = ((Int, Int), [(Int, Int)]) --size of the bitmap, then a list of the bitmaps that are lit

--this compression works by holding the ON bits only, instead of stay storing the OFF bits aswell.
--this compression algorithm is good for sparse images, which means that it has less ON bits than OFF bits.
compress :: Bitmap -> CBitmap
compress bm = ((w, h), on)
 where 
  w = width bm
  h = width bm
  --list of bits that are on
  --taqbad il  bitmap, ticckeja line line, umbad go dak il line,
  --ticcekja kull bit jekk ux on jew le; true jew false. 
  --biex titraversja il lines, ijed tuza il !! fuq il y, u horizontal bil kontra.
  on =
   [(x,y)
   | x <- [0..w-1]
   , y <- [0. h-1]
   , (bm !! y) !! x == True
   ] --'!!' to return the item at that position

--decompress :: CBitmap -> Bitmap

--ps1 and ps2 are the pixels that are ON
--assuming that the size is the same for both images.
intersectCBitmaps :: CBitmap -> CBitmap - CBitmap
intersectCBitmaps (size, ps1) (size', ps2) = (size, ps1 `intersect` ps2)

circle r = ((100, 100), bits)
 where
  r2 = r*r 
  bits = 
  [(x,y)
  | x <- [0..99]
  , y <- [0..99]
  , (x-50)^2 + (y-50)^2 < r2
  ]