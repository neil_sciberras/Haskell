--type String = [Char] --it's just a type synonym, because a string is a list of chars

module A where 

import Data.List
import Data.Char

--this is a partial function, unless you define what take1 of an empty list gives you
take1 :: [a] -> [a]
take1 xs = [head xs]
--take1 (x:xs) = [x]

--takes the first two elements of a list
take2 :: [a] -> [a]
take2 (x:(y:zs)) = [x,y]
--take2 (x:y:zs) = [x,y]
--take2 (x:y:_) = [x,y]

--defining a new operator, 
--the brackets are used because the operator uses symbols
(+++) :: [a] -> [a] -> [a]
[] +++ xs = xs
(y:ys) +++ xs = y:(ys +++ xs)
--the size of the rhs is smaller than the lhs, primitive recursion
--because of this, recursion will always end
--order n, where n is the size of the first list

rev :: [a] -> [a]
rev [] = []
rev (x:xs) = rev xs ++ [x]
--order n^2

null' :: [a] -> Bool
null' [] = True
null' _ = False

--NB. the type of an empty list is the list of any type, [a]

take' :: Int -> [a] -> [a]
take' 0 _ = [] --take' 0 xs = []
take' _ [] = [] --take' n [] = []
take' n (x:xs) = x: take (n-1) xs
--without the second line of the definition, take' 13 [1..10] will not work, because at a point it will become take' n [], but till now it's not defined

--example 12, -12, +3, +3.2, -3.2, -.3
isSignedNumber :: String -> Bool
isSignedNumber ('+':xs) = isUnsignedNumber xs
isSignedNumber ('-':xs) = isUnsignedNumber xs
isSignedNumber xs = isUnsignedNumber xs --ex. 12

isUnsignedNumber xs = isNaturalNumber xs || isReal xs

isNaturalNumber  "" = False --when no number is entered
isNaturalNumber xs = isNaturalNumber' xs
 where 
  isNaturalNumber' "" = True
  isNaturalNumber' (x:xs) = isDigit x && isNaturalNumber' xs --isDigit is in the library

isReal "" = False
isReal ('.':xs) = isNaturalNumber xs
isReal (x:xs) = isDigit x && isReal xs

odd' n = n `mod` 2 == 1

map' :: (a -> b) -> [a] -> [b]
map' f [] = []
map' f (x:xs) = f x: map' f xs

filter' :: (a -> Bool) -> [a] -> [a]
filter' p [] = []
filter' p (x:xs)
 | p x = x: filter' p xs --x: (filter' p xs), because haskell firt analyzes the lhs of the :, then the rhs, and then it applies the :
 | otherwise = filter' p xs

break' :: (a -> Bool) -> [a] -> ([a], [a])
break' p [] = ([], [])
break' p (x:xs)
 | p x = ([], x:xs)
 | otherwise = 
  let (ys, zs) = break' p xs 
  in (x:ys, zs)
 --since break' returns two lists, one is being named ys and the other is named zs.
 
--split, splits a string and removes that character. Also, it splits into multiple lists not just two lists.

all' f [] = False
all' f xs= all f xs 

isDot c = '.' == c

isANumber xs 
 | null post = all' isDigit xs
 | otherwise = all isDigit pre && all' isDigit (tail post) --tail of post, to remove the '.'
 where 
  (pre, post) = break' ('.' ==) xs
  --(pre, post) = break' isDot xs
  
areNumbers input = all isANumber lines
 where 
  lines = split' ('\n'==) input
  

areNumbers' = all isANumber . split' ('\n'==) --clean version
--'input' is removed from both sides
  
--zip' :: [a] -> [b] -> [(a,b)]

--zip [1..10] "Gordon"
--[(1,'G'), (2,'o') and it continues]

areNumbers'' input 
 | null errorLines = "It works"
 | otherwise = "Error in line" ++ show (head errorLines)
 where 
  errorLines = [n | (n,line) <- [1..] lines, not (isNumber line)] --zips 1.. with the list of lines, then choose the pairs where the line is not a number, and remember the line number where the lines are not a number
  lines = split' ('\n'==) input


--foldl and foldr are already defined in the prelude
foldl'' :: (a -> b -> a) -> a -> [b] -> a
foldl'' f x0 [] = x0
foldl'' f x0 (x:xs) = foldl'' f (f x0 x) xs

rev'' xs = foldl'' (\xs x -> x:xs) [] ys


foldr'' :: (b -> a -> a) -> a -> [b] -> a
foldr'' f x0 [] = x0
foldr'' f x0 (x:xs) = f x (foldr'' f x0 xs)