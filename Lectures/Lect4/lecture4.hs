module DataTypes where 

import Data.Maybe
import Data.List

-- Maybe is already defined
{-
data Maybe a = Just a | Nothing deriving (Show, Eq)
-}
--the deriving was telling the compiler that on the data Maybe,
--we can use the functions in the Show and Eq classes.

--takes a function which takes a and outputs a, but does it in a safe way.
--If thre's an error for example, it returns Nothing.
--But if you can compute the output, then output it just normally.
safeUnary :: (a -> b) -> Maybe a -> Maybe b
safeUnary f Nothing = Nothing
safeUnary f (Just x) = Just (f x)

safeBinary : (a -> b -> c) -> Maybe a -> Maybe b -> Maybe c
safeBinary f Nothing _ = Nothing
safeBinary f _ Nothing = Nothing
safeBinary f (Just x) (Just y) = Just (f x y)

{-
--Show is a class where all the types that can be shown as strings are in it.
class Show a where 
 show :: a -> String
 
--Eq is the class where all the types that we can do equality on them, is in it.
class Eq a where 
 (==) :: a -> a -> Bool
-}

 
type VariableName = String --just a type synonym

--expression of integers which will be a part of our language
data Exp
 = Val Integer --constant
 | Var VariableName --variables
 | Add Exp Exp 
 | Mul Exp Exp
 | Sub Exp Exp
 | Neg Exp --negation 
 --deriving (Show) ---- this will show the expression in a not so nice way, so we do another show

--this is just how haskell will do the show
{-
instance Show Exp where
 show (Val n) = "Val " ++ show n
 show (Add e1 e2) = "Add " ++ showBrac e1 ++ showBrac e2
-}

--this is a nicer way to show an expression
instance Show Exp where
 show (Val n) = show n
 show (Var v) = v --we do not do show to avoid the inverted commas
 show (Add e1 e2) = showBrac e1 ++ "+" ++ showBrac e2
 show (Mul e1 e2) = showBrac e1 ++ "*" ++ showBrac e2
 show (Sub e1 e2) = showBrac e1 ++ "-" ++ showBrac e2
 show (Neg e) = "-" ++ showBrac e

--if we can show a, then we can apply showBrac to it. But a type that we cannot apply show on it, then neither 
--we can apply showBrac on it.
showBrac :: Show a => a -> String
showBrac e = "(" ++ show e ++ ")"
 

--the maybe is used, so when there is a variable we dont know, we return nothing
evalSimple :: Exp -> Maybe Integer
evalSimple (Val n) = Just n
evalSimple (Var v) = Nothing
evalSimple (Neg e) = (safeUnary negate) (evalSimple e)
evalSimple (Add e1 e2) = (safeBinary (+)) (evalSimple e1) (evalSimple e2) --instead of "evalSimple e1 + evalSimple e2"
evalSimple (Mul e1 e2) = (safeBinary (*)) (evalSimple e1) (evalSimple e2) 
evalSimple (Sub e1 e2) = (safeBinary (-)) (evalSimple e1) (evalSimple e2)

--we're defining the Eq function on the type Exp that we just defined.
instance Eq Exp where 
 e == e'
  | not(isNothing v) && v==v' = True --v should be Just something, not 'Nothing', also it should be equal to v'.
   where 
    v = evalSimple e
	v' = evalSimple e'
 Val n == Val n' = n==n'
 Var v == Var v' = v==v'
 (Add e1 e2) == (Add e1' e2') = e1==e1' && e2==e2'
 (Mul e1 e2) == (Mul e1' e2') = e1==e1' && e2==e2'
 (Sub e1 e2) == (Sub e1' e2') = e1==e1' && e2==e2'
 (Neg e) == (Neg e') = e==e'
 _ == _ = False
 
 
instance Num Exp where 
 e1 + e2 = Add e1 e2
 e1 - e2 = Sub e1 e2
 e1 * e2 = Mul e1 e2
 negate e = Neg e
 fromInteger n = Val n
 
 

--class Num a where 
-- (+) :: a -> a -> a
--only types that go from a->a to a can be labelled to be a Num
 
--when we do "instance Num Exp where "
--it means that Exp is in the class Num, because Exp goes from a->a to a
 
----------------------------------------------------------------------------------------------------------------- 
 
--imperative program
data Prg
 = Prg :> Prg --Seq Prg Prg 
 | While Exp Prg --if it is zero, it breaks, else it continues
 | IfThenElse Exp Prg Prg --condition, if branch, and else branch
 | VariableName := Exp --Just an infix form of "Assign VariableName Exp". Just syntactic sugar

 
instance Show Prg where 
 show p = unlines (show' p)
  where 
   show' (v := e) = [v ++ "=" ++ show e ]
   show' (p :> p') = show' p ++ show' p'
   show' (While e p) = 
    ["while ("++show' e ++") {" ++
    mp ("   "++) (show' p) ++
    "}"
   show' (IfThenElse e p p') = 
    ["if (" ++ show e ++ ") {"] ++
    map ("   "++) (show' p) ++
    []...........
	
	
	
	
	
--these define the precedence, but not important
infix 0 :>
infix 1 :=
 
x =/= y = (x-y)

fib n = 
 "i" := n :> (
 "a" := 1 :> (
 "b" := 1 :> (
 While (i != 2) (
  "t" := b :> (
  "b" := a+b :> (
  "a" := t :> ( 
  "i" := i-1
 )))))))
  where 
   b = Var "b"
   a = Var "a"
   i = Var "i"
   t = Var "t"
 

--this gives you the value of the variables
type Mem = VariableName -> Integer
 
update :: Mem -> (VariableName, Integer) -> Mem
update mem (v,n) = \w -> if(w==v) then n else mem w

evaluate :: Mem -> Exp -> Integer
evaluate _ (Val n) = n
evaluate mem (Var v) = mem v
evaluate mem (Neg e) = - (evaluate mem e)
evaluate mem (Add e1 e2) = evaluate mem e1 + evaluate mem e2
evaluate mem (Mul e1 e2) = evaluate mem e1 * evaluate mem e2
evaluate mem (Sub e1 e2) = evaluate mem e1 - evaluate mem e2

run :: Prg -> Mem -> Mem
run (v := e) mem = update mem (v, evaluate mem e)
run (p :> p') mem = run p' (run p mem)
run (IfThenElse e p p') mem
 |evaluate mem e /= 0 = run p mem 
 |otherwise = run p' mem
run (While e p) mem
 | evaluate mem e /= 0 = run (p :> While e p) mem
 |otherwise            = mem
--ex. run (fib 3) (\v -> 0) "b"




 
--   "-(x*3)"
ex1 = (Mul (Var "x") (Val 3))
ex2 = Neg (Mul (Val 2) (Val 3))
ex3 = Neg (Mul (Val 1) (Val 6))

ex1' = -(Var "x" * 3 )
--when you do show example