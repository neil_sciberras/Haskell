module Test where

import Debug.Trace
import Data.List

dbl :: Float -> Float
dbl x = x+x

dbl' :: Float -> Float
dbl' = \x -> x+x --lambda abstraction
--bdl' x = x+x

quad :: Float -> Float
quad x = dbl (dbl x)
quad' x =
  let x' = dbl x
  in  dbl x'
  
quad'' x =
  dbl x'
    where
      x' = dbl x

--dbl.dbl is function composition
quad''' x = (dbl . dbl) x

--the same as quad''', but 'x' can be omitted
quad'''' = dbl . dbl


fib 0 = 1
fib 1 = 1
fib n = fib (n-1) + fib (n-2)

fib' n
  | n <= 1    = 1
  | otherwise = fib' (n-1) + fib' (n-2)

power (x, 0) = 1
power (x, n) = x * power (x,n-1)

sqrt1 :: Double -> Double
sqrt1 n = sqrt0 (0,n)
  where
    epsilon = 0.01
    sqrt0 (from, to)
      | abs (m*m - n) <= epsilon = m
      | m*m < n                  = sqrt0 (m, to)
      | otherwise                = sqrt0 (from, m)
      where
        m = (from+to) / 2


--applying a function f on x, for a number of times 'n'
--a recursive definition
applyN :: (Float -> Float, Int, Float) -> Float
applyN (f, 0, x) = x
applyN (f, n, x) = f (applyN (f,n-1,x))

sqr' :: Float -> Float
sqr' x = x*x

nand :: (Bool,Bool) -> Bool
nand (x,y) = not (x && y)

nand' :: Bool -> Bool -> Bool
nand' x y = not (x && y)

f :: Integer -> Integer
f x = 7


