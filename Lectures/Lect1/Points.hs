module Points where

--type synonyms
type Distance = Double
type Angle = Double

type Cartesian = (Distance, Distance)
type Polar = (Distance, Angle)

--sqr's type is automatically inferred
sqr x = x*x

--type explicitly set. Also, this function is curried
distanceC :: Cartesian -> Cartesian -> Distance
distanceC (x0,y0) (x1,y1) = sqrt (sqr (x1-x0) + sqr (y1-y0))

--guards are used in angleC
angleC :: Cartesian -> Angle
angleC (x,y)
  | x == 0    = if y>=0 then pi/2 else -pi/2
  | otherwise = (if x<0 then pi else 0) + atan (y/x)

translateC :: Cartesian -> Cartesian -> Cartesian
translateC (x0,y0) (x1,y1) = (x0+x1, y0+y1)

rotateC :: Angle -> Cartesian -> Cartesian
-- rotateC theta (x,y) = p2c (rotateP theta (c2p (x,y)))
rotateC theta = p2c . rotateP theta . c2p

rotateP :: Angle -> Polar -> Polar
rotateP theta (r,alpha) = (r, alpha+theta)

origin = (0,0)

c2p :: Cartesian -> Polar
c2p p = (origin `distanceC` p, angleC p)

p2c :: Polar -> Cartesian
p2c (r,alpha) = (r*cos alpha, r*sin alpha)

type Tri = (Cartesian, Cartesian, Cartesian)

invert (x,y) = (-x,-y)

area :: Tri -> Double
area (p1,p2,p3) =
  let p2' = translate p2 (invert p1)
      p3' = translate p3 (invert p1)

      (_,alpha) = c2p p3'

      (x,_) = rotateC (-alpha) p2'
      (_,y) = rotateC (-alpha) p3'
  in  x*y/2

